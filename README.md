# 99 Bottles

Disclaimer from the book, [99 Bottles of OOP](https://www.sandimetz.com/99bottles/).
> 99 Bottles of OOP is a practical guide to writing cost-effective, maintainable, and pleasing object-oriented code. 

This repository is an attempt to solve exercise in Python. The exercise is originally available in Ruby at [https://github.com/sandimetz/99bottles](https://github.com/sandimetz/99bottles)


## Run Tests

By default, [nose2](http://nose2.readthedocs.io/en/latest/index.html) is selected as the default test runner.

```
$ nose2 test.bottles
```
